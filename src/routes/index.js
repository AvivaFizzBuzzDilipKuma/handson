const router = [
    {
        path: '*',
        component: () => import('../components/PageNotFound.vue'),
        meta: { auth: false }
    },
    {
        name: 'login',
        path: '/login',
        component: () => import('../components/Login.vue'),
        meta: { auth: false }
    },
    {
        name: 'home',
        path: '/home',
        component: () => import('../components/Home.vue'),
        meta: { auth: true }

    },
    {
        path: '/',
        redirect: '/login'
    }
]


export default router;